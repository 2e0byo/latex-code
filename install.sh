#!/bin/sh
echo "Installing all .sty files in current dir to users TEXMFHOME"
t=$(kpsewhich -var-value TEXMFHOME)
mkdir -p $t
for i in "$(ls *.sty)"; do
    mkdir -p $t/tex/latex/local/"${i%.*}"
    ln -sr $i $t/tex/latex/local/"${i%.*}"/$i
done
